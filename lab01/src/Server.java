
import java.math.BigInteger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.HashSet;
import org.json.*;


class Listener{
    private DataOutputStream out;
    private BigInteger secret;
    private String cipherName;
    private Base64.Encoder base64encoder = Base64.getEncoder();

    public Listener(DataOutputStream out, BigInteger secret, String cipherName) {
        this.out = out;
        this.secret = secret;
        this.cipherName = cipherName;
    }

    public void setCipher(String cipherName){
        this.cipherName = cipherName;
    }

    public void sendMessage(String message, String senderName){
        try {
            byte[] encryptedMessage = encrypt(message);
            String encrypted = base64encoder.encodeToString(encryptedMessage);
            out.writeUTF("{\"msg\":" + "\"" + encrypted + "\",\"from\":" + senderName + "}");
        } catch (IOException e) {
            System.out.println("Message cannot be sent !");
        }
    }

    private byte[] encrypt(String message){
        switch (this.cipherName) {
            case "xor":
                return xorCipherEncrypt(message, secret);
            case "cezar":
                return cezarCipherEncrypt(message, secret.mod(Server.CEZAR_ASCII_MODULO));
            default:
                return message.getBytes();
        }
    }

    byte[] cezarCipherEncrypt(String message, BigInteger shiftBig){
        int shift = shiftBig.intValue();
        byte[] messageBytes = message.getBytes();
        for(int x = 0; x < messageBytes.length; x++){
            if(messageBytes[x] + shift > Server.LAST_ASCII_CHARACTER)
                messageBytes[x] -= Server.CEZAR_ASCII_OFFSET;
            messageBytes[x] += shift;
        }
        return messageBytes;
    }

    byte[] xorCipherEncrypt(String message, BigInteger secret) {
        byte secretByte = secret.byteValue();
        byte [] messageBytes = message.getBytes();
        for(int i=0; i< messageBytes.length; i++)
            messageBytes[i]= (byte)(messageBytes[i] ^ secretByte);
        return messageBytes;
    }
}

public class Server {

    private static final int PORT = 9001;

    private static HashSet<Listener> listeners = new HashSet<>();

    static final int FIRST_ASCII_CHARACTER = 32;
    static final int LAST_ASCII_CHARACTER = 126;
    static final int CEZAR_ASCII_OFFSET = 95;
    static final BigInteger CEZAR_ASCII_MODULO = BigInteger.valueOf(94);

    public static void main(String[] args) throws Exception {

        System.out.println("The chat server is running.");
        ServerSocket listener = new ServerSocket(PORT);
        try {
            while (true) {
                new Handler(listener.accept()).start();
            }
        } finally {
            listener.close();
        }
    }

    private static BigInteger generateP(){
        int bitLength = 128;
        SecureRandom rnd = new SecureRandom();
        return BigInteger.probablePrime(bitLength, rnd);
    }
 
    private static BigInteger generateG(){
        int bitLength = 128;
        SecureRandom rnd = new SecureRandom();
        return BigInteger.probablePrime(bitLength, rnd);
    }
 
    private static BigInteger generateSecretlyB(){
        int bitLength = 128;
        SecureRandom rnd = new SecureRandom();
        return BigInteger.probablePrime(bitLength, rnd);
    }
 
    private static BigInteger countB(BigInteger g, BigInteger b, BigInteger p){
 	   BigInteger B = g.modPow(b, p);
 	   return B;
    }

    static String cezarCipherDecrypt(byte[] messageBytes, BigInteger shiftBig){
        int shift = shiftBig.intValue();
        for(int x = 0; x < messageBytes.length; x++){
            if(messageBytes[x] - shift < FIRST_ASCII_CHARACTER)
                messageBytes[x] += CEZAR_ASCII_OFFSET;
            messageBytes[x] -= shift;
        }
        return new String(messageBytes);
    }

    static String xorCipherDecrypt(byte[] messageBytes, BigInteger secret){
        byte secretByte = secret.byteValue();
        for(int i=0; i< messageBytes.length; i++)
            messageBytes[i]= (byte)(messageBytes[i] ^ secretByte);
        return new String(messageBytes);
    }

    private static BigInteger countS(BigInteger A, BigInteger b, BigInteger p ){
	   return A.modPow(b, p);
    }


    private static void sendPandGtoClient(BigInteger p, BigInteger g, DataOutputStream out) throws IOException {
        out.writeUTF("{\"p\":" + p + ", \"g\":" + g + "}");
    }
    
    private static String decrypt(byte[] msg, String cipherName, BigInteger secret){
        switch (cipherName) {
            case "xor":
                return xorCipherDecrypt(msg, secret);
            case "cezar":
                return cezarCipherDecrypt(msg, secret.mod(CEZAR_ASCII_MODULO));
            default:
                return new String(msg);                 // no ciphering
        }

    }
    private static class Handler extends Thread {
        private Socket socket;
        private DataInputStream in;
        private DataOutputStream out;
        private Listener listener;
        private BigInteger secret;
        private String cipherName = "none";
        private JSONObject jsonDecoder;
        private Base64.Decoder base64decoder = Base64.getDecoder();

        public Handler(Socket socket) {
            this.socket = socket;
        }

        private void connectWithTheClient() throws IOException {

            while (true) {
                String st = in.readUTF();
                jsonDecoder = new JSONObject(st);

                if (jsonDecoder.has("request") && jsonDecoder.getString("request").contains("keys")) {
                    BigInteger p = generateP();
                    BigInteger g = generateG();
                    sendPandGtoClient(p, g, out);

                    BigInteger b = generateSecretlyB();
                    BigInteger B = countB(g, b, p);
                    String resp = in.readUTF();
                    BigInteger A = new JSONObject(resp).getBigInteger("a");
                    out.writeUTF("{\"b\" : " + B + "}");
                    secret = countS(A, b, p);
                    System.out.println("New user connected with secret :" + secret);
                    return;
                }
            }
        }

        private void readAndBroadcastMessage(String input){
            jsonDecoder = new JSONObject(input);
            String message = jsonDecoder.getString("msg");
            String senderName = jsonDecoder.getString("from");
            System.out.println("encrypted message in base64: " + message + " from: " + senderName);

            byte[] decodedMessage = base64decoder.decode(message);
            String decryptedMessage = decrypt(decodedMessage, cipherName, secret);

            System.out.println("from: " + " : " + senderName + " , msg: " + decryptedMessage);

            for (Listener listener : listeners) {
                listener.sendMessage(decryptedMessage, senderName);
            }
        }

        private void changeCiphering(String cipherName){
            listeners.remove(listener);
            this.cipherName = cipherName;
            listener.setCipher(cipherName);
            listeners.add(listener);

            System.out.println("changed to " + cipherName);
        }

        public void run() {
            try {

                // Create character streams for the socket.
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());

                connectWithTheClient();

                listener = new Listener(out, secret, cipherName);
                listeners.add(listener);
                
                while (true) {
                    String input = in.readUTF();
                    jsonDecoder = new JSONObject(input);

                    if(jsonDecoder.has("msg")) {
                        readAndBroadcastMessage(input);
                    }
                    else if (jsonDecoder.has("encryption")){
                        changeCiphering(jsonDecoder.getString("encryption"));
                    }
                 }
            } catch (Exception e) {
                System.out.println("user disconnected");
            }
            finally {
                if (out != null) {
                    listeners.remove(listener);
                }
                try {
                    socket.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
