
import java.math.BigInteger;
import java.io.*;
import java.awt.*;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.Base64;
import javax.swing.*;
import org.json.*;

public class Client {

    final int FIRST_ASCII_CHARACTER = 32;
    final int LAST_ASCII_CHARACTER = 126;
    final int CEZAR_ASCII_OFFSET = 95;
    final BigInteger CEZAR_ASCII_MODULO = BigInteger.valueOf(94);
    private DataInputStream in;
    private DataOutputStream out;
    private String clientName;
    private JFrame frame = new JFrame("Safe Chatter");
    private JTextField textField = new JTextField(20);
    private JTextArea messageArea = new JTextArea(40, 40);
    private String cipherName="none";
    private BigInteger secret;
    private Base64.Encoder base64encoder = Base64.getEncoder();
    private Base64.Decoder base64decoder = Base64.getDecoder();
    private JSONObject jsonDecoder;
    private final int PORT = 9001;


    public Client() {
        textField.setEditable(false);
        messageArea.setEditable(false);
        frame.getContentPane().add(textField, "South");
        frame.getContentPane().add(new JScrollPane(messageArea), "Center");

        ButtonGroup group = new ButtonGroup();

        JRadioButton noneCipherButton = new JRadioButton("none");
        noneCipherButton.addActionListener(actionEvent -> setCiphering("none"));
        noneCipherButton.setSelected(true);

        JRadioButton xorCipherButton = new JRadioButton("xor");
        xorCipherButton.addActionListener(actionEvent -> setCiphering("xor"));

        JRadioButton cezarCipherButton = new JRadioButton("cezar");
        cezarCipherButton.addActionListener(actionEvent -> setCiphering("cezar"));

        group.add(noneCipherButton);
        group.add(xorCipherButton);
        group.add(cezarCipherButton);
        JPanel radioPanel = new JPanel(new FlowLayout());
        radioPanel.add(noneCipherButton);
        radioPanel.add(xorCipherButton);
        radioPanel.add(cezarCipherButton);
        frame.getContentPane().add(radioPanel, "East");
        frame.pack();

        textField.addActionListener(e -> {
            try {
                sendMessageToServer(textField.getText());
                textField.setText("");
            } catch (IOException e1) {
                messageArea.append("Message cannot be sent !");
            }
        });
    }

    public static void main(String[] args) throws Exception {
        Client client = new Client();
        client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        client.frame.setVisible(true);
        client.run();
    }

    private static BigInteger generateSecretlyA(){
      int bitLength = 128;
      SecureRandom rnd = new SecureRandom();
      return BigInteger.probablePrime(bitLength, rnd);
}

    private static BigInteger countA(BigInteger g, BigInteger a, BigInteger p){
        return g.modPow(a, p);
    }

    private static BigInteger countS(BigInteger B, BigInteger a, BigInteger p ){
	   return B.modPow(a, p);
    }

    private void run() throws IOException, JSONException {

        String serverAddress = getServerAddress();
        Socket socket = new Socket(serverAddress, PORT);
        in = new DataInputStream(socket.getInputStream());
        out = new DataOutputStream(socket.getOutputStream());
        clientName = getName();

        connectWithServer();

        textField.setEditable(true);

        while (true)
            messageArea.append(getMessageFromServer());
    }
    
    private void setCiphering(String cipherName){
        try {
            this.cipherName = cipherName;
            out.writeUTF("{\"encryption\" : \"" + cipherName + "\"}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getServerAddress() {
        String address = JOptionPane.showInputDialog(
                frame,
                "Enter IP Address of the Server: (empty = localhost)",
                "Welcome to the Chatter",
                JOptionPane.QUESTION_MESSAGE);
        return address.isEmpty() ? "localhost" : address;
    }

    private String getName() {
        return JOptionPane.showInputDialog(
                frame,
                "Choose a screen name:",
                "Screen name selection",
                JOptionPane.PLAIN_MESSAGE);
    }

    byte[] cezarCipherEncrypt(String message, BigInteger shiftBig){
        byte[] messageBytes = message.getBytes();
        int shift = shiftBig.intValue();
        for(int x = 0; x < messageBytes.length; x++){
            if(messageBytes[x] + shift > LAST_ASCII_CHARACTER)
                messageBytes[x] -= CEZAR_ASCII_OFFSET;
            messageBytes[x] += shift;
        }
        return messageBytes;
    }

    String cezarCipherDecrypt(byte[] messageBytes, BigInteger shiftBig){
        int shift = shiftBig.intValue();
        for(int x = 0; x < messageBytes.length; x++){
            if(messageBytes[x] - shift < FIRST_ASCII_CHARACTER)
                messageBytes[x] += CEZAR_ASCII_OFFSET;
            messageBytes[x] -= shift;
        }
        return new String(messageBytes);
    }

     byte[] xorCipherEncrypt(String message, BigInteger secret) {
        byte secretByte = secret.byteValue();
        byte [] messageBytes = message.getBytes();
        for(int i=0; i< messageBytes.length; i++)
            messageBytes[i]= (byte)(messageBytes[i] ^ secretByte);
        return messageBytes;
    }

     String xorCipherDecrypt(byte[] messageBytes, BigInteger secret){
         byte secretByte = secret.byteValue();
         for(int i=0; i< messageBytes.length; i++)
             messageBytes[i]= (byte)(messageBytes[i] ^ secretByte);
        return new String(messageBytes);
    }

    private String decrypt(byte[] messageBytes){
        switch (this.cipherName){
            case "xor":
                return xorCipherDecrypt(messageBytes, secret);
            case "cezar":
                return cezarCipherDecrypt(messageBytes, secret.mod(CEZAR_ASCII_MODULO));
            default:
                return new String(messageBytes);            // no ciphering
        }
    }

    private byte[] encrypt(String message){
        switch (this.cipherName){
            case "xor":
                return xorCipherEncrypt(message, secret);
            case "cezar":
                return cezarCipherEncrypt(message, secret.mod(CEZAR_ASCII_MODULO));
            default:
                return message.getBytes();                  // no ciphering
        }
    }

    private void connectWithServer() throws IOException {
        out.writeUTF("{\"request\": \"keys\"}");
        String key = in.readUTF();
        jsonDecoder = new JSONObject(key);
        BigInteger p = jsonDecoder.getBigInteger("p");
        BigInteger g = jsonDecoder.getBigInteger("g");      // now p and g are known

        BigInteger a = generateSecretlyA();                 //random a number
        BigInteger A = countA(g, a, p);
        out.writeUTF("{\"a\" : " + A + "}");                // client sends A number to server

        String response = in.readUTF();
        BigInteger B = new JSONObject(response).getBigInteger("b");

        secret = countS(B, a, p);
        messageArea.append("secret number : " + secret + "\n");
    }

    private String getMessageFromServer() throws IOException {
        String line = in.readUTF();
        jsonDecoder = new JSONObject(line);
        String readMessage="";
        if(jsonDecoder.has("msg")){
            byte[] decodedMessage = base64decoder.decode(jsonDecoder.getString("msg"));
            String decryptedMessage = decrypt(decodedMessage);
            String senderName = jsonDecoder.getString("from");
            readMessage = senderName + ": " + decryptedMessage + "\n";
        }
        return readMessage;
    }

    private void sendMessageToServer(String message) throws IOException {
        byte[] textToSend = encrypt(message);
        String encrypted = base64encoder.encodeToString(textToSend);
        System.out.println("sending message:" + encrypted);
        out.writeUTF("{\"msg\" : \"" + encrypted + "\", \"from\": " + clientName +"}");
    }
}